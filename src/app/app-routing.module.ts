import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ProduitComponent} from './produit/produit.component';
import {ContainerComponent} from './home/container/container.component';


const routes: Routes = [{
  path: '',
  component: HomeComponent,
  children: [{path: '', component: ContainerComponent}, {path: 'produit', component: ProduitComponent}]
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

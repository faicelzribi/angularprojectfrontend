import { Component, OnInit } from '@angular/core';
import {ProduitsService} from '../services/produits.service';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.css']
})
export class ProduitComponent implements OnInit {

  constructor(private produitsService: ProduitsService) { }

  ngOnInit() {
    this.produit();
  }
produit() {
    this.produitsService.getAll().subscribe(res => {
      console.log(res);
    });
}
}
